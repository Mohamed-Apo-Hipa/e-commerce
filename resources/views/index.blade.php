@include('layouts/header')
@include('layouts/menue')

<div class="right_col" role="main">
    @yield('content')
</div>


@include('layouts/footer')