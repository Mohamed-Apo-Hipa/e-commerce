@extends('index')
@section('content')

<div class="x_panel">
  <div class="x_content">
    <div class="x_title">
        <h4><i class="fa fa-cogs"></i> @lang('admin.setting')</h4> 
    </div>
  {!! Form::open(['route'=>('settings.store'),'method'=>'post','files'=>true]) !!}
    <!-- start accordion -->
    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title"><li class="fa fa-wrench"></li> | @lang('admin.sitename_ar') | @lang('admin.sitename_en') | @lang('admin.email') </h4>
        </a>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">

              <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      {!! Form::label('sitename_ar',trans('admin.sitename_ar')) !!}
                      {!! Form::text('sitename_ar',setting()->sitename_ar,['class'=>'form-control']) !!}
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      {!! Form::label('sitename_en',trans('admin.sitename_en')) !!}
                      {!! Form::text('sitename_en',setting()->sitename_en,['class'=>'form-control']) !!}
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      {!! Form::label('email',trans('admin.email')) !!}
                      {!! Form::email('email',setting()->email,['class'=>'form-control']) !!}
                    </div>
                  </div>
                </div> {{-- end of row --}}

          </div>
        </div>
      </div> {{-- end of panel --}}

      <div class="panel">
        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <h4 class="panel-title"><li class="fa fa-wrench"></li> | @lang('admin.logo') | @lang('admin.icon')</h4>
        </a>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
            
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('logo',trans('admin.logo')) !!}
                      <div class="row">
                        <div class="col-md-9">
                          {!! Form::file('logo',['class'=>'form-control image']) !!}
                        </div>
                        <div class="col-md-3">
                          @if(!empty(setting()->logo))
                           <img src="{{Storage::url(setting()->logo) }}" style="width:50px;height: 50px;" class="image-preview" />
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>  {{-- end of col --}}
                  <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('icon',trans('admin.icon')) !!}
              
                      <div class="row">
                        <div class="col-md-9">
                          {!! Form::file('icon',['class'=>'form-control image1']) !!}
                        </div>
                        <div class="col-md-3">                
                          @if(!empty(setting()->icon))
                            <img src="{{ Storage::url(setting()->icon) }}" style="width:50px;height: 50px;" class="image-preview1" />
                          @endif
                        </div>
                      </div>
                    </div>
                  </div> {{-- end of col --}}
                </div> {{-- end of row --}}
            
          </div>
        </div>
      </div> {{-- end of panel --}}


      <div class="panel">
        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapseTwo">
          <h4 class="panel-title"><li class="fa fa-wrench"></li> | @lang('admin.description') | @lang('admin.keywords')</h4>
        </a>
        <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
                
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('description',trans('admin.description')) !!}
                      {!! Form::textarea('description',setting()->description,['class'=>'form-control' ]) !!}
                    </div>
                </div>{{-- end of col --}}
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('keywords',trans('admin.keywords')) !!}
                    {!! Form::textarea('keywords',setting()->keywords,['class'=>'form-control']) !!}
                  </div>
                </div>{{-- end of col --}}
              </div>{{-- end of row --}}

          </div>
        </div>
      </div> {{-- end of panel --}}


      <div class="panel">
        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapseTwo">
          <h4 class="panel-title"><li class="fa fa-wrench"></li> | @lang('admin.main_lang') | @lang('admin.status')      </h4>
        </a>
        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
            
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('main_lang',trans('admin.main_lang')) !!}
                      {!! Form::select('main_lang',['ar'=>trans('admin.ar'),'en'=>trans('admin.en')],setting()->main_lang,['class'=>'form-control','style'=>'height:38px']) !!}
                    </div>
                  </div>{{-- end of col --}}
                  <div class="col-md-6">
                    <div class="form-group">
                     {!! Form::label('status',trans('admin.status')) !!}
                     {!! Form::select('status',['open'=>trans('admin.open'),'close'=>trans('admin.close')],setting()->status,['class'=>'form-control','style'=>'height:38px']) !!}
                   </div>
                  </div>{{-- end of col --}}
                </div>{{-- end of row --}}

                <div class="form-group">
                  {!! Form::label('message_maintenance',trans('admin.message_maintenance')) !!}
                  {!! Form::textarea('message_maintenance',setting()->message_maintenance,['class'=>'form-control','id'=>'editor']) !!}

                </div>  {{-- message maintenace --}}

          </div>
        </div>
      </div> {{-- end of panel --}}

    </div>
    <!-- end of accordion -->
    <button type="submit" class="btn btn-primary"><i class="fa fa-gear"></i> @lang('admin.save')</button>
  {!! Form::close() !!}  

    
  </div> {{-- end of x-content --}}
</div> {{-- end of x-panel --}}
@endsection


