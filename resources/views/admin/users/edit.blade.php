@extends('index')
@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><i class="fa fa-edit"></i> {{ $title }}</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {!! Form::open(['route'=>['user.update', $user->id],'id'=>'form_data','method'=>'PUT']) !!}
                <div class="form-group">
                    {!! Form::label('name', __('admin.name')) !!}
                    {!! Form::text('name',$user->name,[
                        'class'=>'form-control',
                        'required'=>'required',
                        'data-validation'=>'length',
                        'data-validation-length'=>'min5',
                        'data-validation-error-msg'=> __('admin.lengtherror')
                    ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', __('admin.email')) !!}
                    {!! Form::email('email',$user->email,[
                        'class'=>'form-control',
                        'required'=>'required',
                        'data-validation'=>'email',
                        'data-validation-error-msg' => __('admin.emailerror')
                        ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', __('admin.password')) !!}
                    {!! Form::password('password',[
                        'class'=>'form-control',
                    ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('level', __('admin.level')) !!}
                    {!! Form::select('level',
                    [   'user' => __('admin.user'),
                        'company' => __('admin.company'),
                        'vendor' => __('admin.vendor'),
                    ],$user->level,
                    [   'class'=>'form-control',
                        'required'=>'required',
                        'placeholder'   => ' ',
                        'style'     => 'height:38px'
                    ]) !!}
                </div>
                <button type="submit" id='btn' class="btn btn-primary">
                    <i class="fa fa-edit"></i> @lang('admin.update')
                </button>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')
<script>
        $.validate({
            modules : 'location, date, security, file',
        });
</script>
@endpush
@endsection