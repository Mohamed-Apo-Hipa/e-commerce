<span
class="label
{{ $level == 'user'?'label-info':'' }}
{{ $level == 'company'?'label-success':'' }}
{{ $level == 'vendor'?'label-primary':'' }}
">
    @lang('admin.'.$level)
</span>