
{!! Form::open(['route'=>['user.destroy', $id], 'method'=>'delete' , 'style'=>'display: inherit']) !!}
    <button class="btn btn-danger btn-sm delete" ><i class="fa fa-trash fa-lg"></i></button>
{!! Form::close() !!}