<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>{{ trans('admin.resetpassword') }}</title>

    <!-- Bootstrap -->
    <link href="{{ url('') }}/design/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('') }}/design/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('') }}/design/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ url('') }}/design/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="{{ url('') }}/design/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ url('') }}/design/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ url('') }}/design/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    

    <!-- Custom Theme Style -->
    <link href="{{ url('') }}/design//build/css/custom.min.css" rel="stylesheet">
    <script src="{{ url('') }}/design/build/js/sweetalert.js"></script>
    <style>
        input[type=checkbox]{
          height: 0;
          width: 0;
          visibility: hidden;
        }
        
        label {
          cursor: pointer;
          text-indent: -9999px;
          width: 50px;
          height: 25px;
          background: grey;
          border-radius: 100px;
          position: relative;
        }
        label:after {
          content: '';
          position: absolute;
          top: 2px;
          left: 2px;
          width: 22.5px;
          height: 22.5px;
          background: #fff;
          border-radius: 90px;
          transition: 0.1s;
        }
        
        input:checked + label {
          background: #337ab7;;
        }
        
        input:checked + label:after {
          left: calc(100%);
          transform: translateX(-100%);
        }
        
        label:active:after {
          width: 30px;
        }
        
        // centering
        body {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100vh;
        }
    </style>
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
    <div class="login">
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            
            {!! Form::open(['method'=>'POST','aurl'=>'reset/password']) !!}
              <h1>{{ trans('admin.resetpassword') }}</h1>
              <div class="form-group">
                <input type="email" class="form-control" value="{{ $data->email }}">
              </div>
              <div class="form-group">
                {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
              </div>
              <div class="form-group">
                {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Password Confirmation']) !!}
              </div>
              <div>
                  <button type="submit" class="btn btn-primary submit" ><i class="fa fa-refresh fa-lg " ></i> Reset </button>
                </div>
            {!! Form::close() !!}
          </section>
        </div>
      </div>
    </div>
    </div>
    </div>
    <!-- jQuery -->
    <script src="{{ url('') }}/design/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{ url('') }}/design/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="{{ url('') }}/design/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="{{ url('') }}/design/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="{{ url('') }}/design/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="{{ url('') }}/design/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ url('') }}/design/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="{{ url('') }}/design/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="{{ url('') }}/design/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="{{ url('') }}/design/vendors/Flot/jquery.flot.js"></script>
    <script src="{{ url('') }}/design/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="{{ url('') }}/design/vendors/Flot/jquery.flot.time.js"></script>
    <script src="{{ url('') }}/design/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="{{ url('') }}/design/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="{{ url('') }}/design/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="{{ url('') }}/design/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="{{ url('') }}/design/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="{{ url('') }}/design/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="{{ url('') }}/design/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="{{ url('') }}/design/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="{{ url('') }}/design/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ url('') }}/design/vendors/moment/min/moment.min.js"></script>
    <script src="{{ url('') }}/design/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{ url('') }}/design/build/js/custom.min.js"></script>
    
    </body>
    </html>