@extends('index')
@section('content')
<h3><i class="fa fa-users"></i> {{$title}}</h3>
<div class='panel panel-primary'>
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        {!! Form::open([ 'id'=>'form_data','url'=>aurl('admin/destroy/all'), 'method'=>'delete' ]) !!}
            {!! $dataTable->table(['class'=>'table table-striped table-bordered dt-responsive nowrap','cellspacing'=>'0','width' =>'100%'],true) !!}
        {!! Form::close() !!}
    </div>
</div>
{{-- Start model multi Delete  --}}
<!-- Button trigger modal -->
  <div class="modal fade" id="multiDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">@lang('admin.delete_all')</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger">
                <div class="empty_record hidden">
                    <h4>@lang('admin.please_check_some_recordes')</h4>
                </div>
                <div class="not_empty_record hidden">
                    <h4> @lang('admin.ask_delete_item') <span class="record_count"> ?</span></h4>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="empty_record hidden">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('admin.close')</button>
                </div>
                <div class="not_empty_record hidden">   
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> @lang('admin.no')</button>  
                    <input type="submit" value="@lang('admin.yes') " class="btn btn-danger del_all">
                </div>
        </div>
      </div>
    </div>
  </div>
{{-- End model multi Delete  --}}
@push('js')
<script>
    delete_all();
</script>

{!! $dataTable->scripts() !!}
@endpush
@endsection
