@extends('index')
@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><i class="fa fa-plus-circle"></i> {{ $title }}</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {!! Form::open(['route'=>('admin.store'), 'method'=>'post']) !!}
                <div class="form-group">
                    {!! Form::label('name', trans('admin.name')) !!}
                    {!! Form::text('name', old('name'),[
                        'class'=>'form-control',
                        'required'=>'required',
                        'data-validation'=>'length',
                        'data-validation-length'=>'min5',
                        'data-validation-error-msg'=> trans('admin.lengtherror')
                    ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', trans('admin.email')) !!}
                    {!! Form::email('email', old('email'),[
                        'class'=>'form-control',
                        'required'=>'required',
                        'data-validation'=>'email',
                        'data-validation-error-msg' => trans('admin.emailerror')
                        ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', trans('admin.password')) !!}
                    {!! Form::password('password',[
                        'class'=>'form-control',
                        'data-validation'=>'length',
                        'required'=>'required',
                        'data-validation-length'=>'min8',
                        'data-validation-error-msg' => trans('admin.passworderror')
                    ]) !!}
                </div>
                <button type="submit"id='btn' class="btn btn-primary">
                    <i class="fa fa-plus-circle"></i> {{trans('admin.create')}}
                </button>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>
        $.validate({
            modules : 'location, date, security, file',
        });
</script>
@endpush
@endsection