@extends('index')
@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><i class="fa fa-edit"></i> {{ $title }}</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {!! Form::open(['route'=>['admin.update', $admin->id],'id'=>'form_data','method'=>'PUT']) !!}
                <div class="form-group">
                    {!! Form::label('name', trans('admin.name')) !!}
                    {!! Form::text('name',$admin->name,[
                        'class'=>'form-control',
                        'required'=>'required',
                        'data-validation'=>'length',
                        'data-validation-length'=>'min5',
                        'data-validation-error-msg'=> trans('admin.lengtherror')
                    ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', trans('admin.email')) !!}
                    {!! Form::email('email',$admin->email,[
                        'class'=>'form-control',
                        'required'=>'required',
                        'data-validation'=>'email',
                        'data-validation-error-msg' => trans('admin.emailerror')
                        ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', trans('admin.password')) !!}
                    {!! Form::password('password',[
                        'class'=>'form-control',
                    ]) !!}
                </div>
                <button type="submit" id='btn' class="btn btn-primary">
                    <i class="fa fa-edit"></i> {{trans('admin.update')}}
                </button>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')
<script>
        $.validate({
            modules : 'location, date, security, file',
        });
</script>
@endpush
@endsection