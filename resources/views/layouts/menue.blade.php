<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
      <div class="navbar nav_title" style="border: 0;">
        <a href="index.html" class="site_title"><i class="fa fa-cart-plus"></i> <span>e-commerce</span></a>
      </div>

      <div class="clearfix"></div>

      <!-- menu profile quick info -->
      <div class="profile clearfix">
        <div class="profile_pic">
          <img src="{{ url('') }}/design/images/img.jpg" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
          <span>Welcome</span>
          <h2>{{ admin()->user()->name }}</h2>
        </div>
      </div>
      <!-- /menu profile quick info -->

      <br />

      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
          <ul class="nav side-menu">
            {{-- dashboard / settings --}}
            <li><a><i class="fa fa-tachometer"></i> {{trans('admin.dashboard')}}<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                  <li><a href="{{ aurl("") }}"><i class="fa fa-tachometer"></i> {{trans('admin.dashboard')}} </a>
                  <li><a href="{{ aurl("settings") }}"><i class="fa fa-cogs"></i> {{trans('admin.setting')}} </a>
              </ul>
            </li>

              {{--  admin  --}}
            <li><a><i class="fa fa-users"></i> {{trans('admin.adminAccount')}}<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                    <li><a href='{{ aurl("admin") }}'><i class="fa fa-edit"></i> {{trans('admin.admin')}} </a>
              </ul>
            </li>
                {{--  users  --}}
              <li><a><i class="fa fa-user-plus"></i> {{trans('admin.users')}}<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                      <li><a href='{{ aurl("user") }}'><i class="fa fa-edit"></i> {{trans('admin.users')}} </a>
                      <li><a href='{{ aurl("user") }}?level=user'><i class="fa fa-edit"></i> {{trans('admin.user')}} </a>
                      <li><a href='{{ aurl("user") }}?level=company'><i class="fa fa-edit"></i> {{trans('admin.company')}} </a>
                      <li><a href='{{ aurl("user") }}?level=vendor'><i class="fa fa-edit"></i> {{trans('admin.vendor')}} </a>
                </ul>
              </li>

          </ul>
        </div>

      </div>
      <!-- /sidebar menu -->

      <!-- /menu footer buttons -->
      <div class="sidebar-footer hidden-small">
        <a href="{{ aurl('settings') }}" data-toggle="tooltip" data-placement="top" title="Settings">
          <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
          <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
          <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ aurl('logout') }}">
          <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
      </div>
      <!-- /menu footer buttons -->
    </div>
  </div>

  <!-- top navigation -->
  <div class="top_nav">
    <div class="nav_menu">
      <nav>
        <div class="nav toggle">
          <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>

        <ul class="nav navbar-nav navbar-right">
        {{-- profile --}}
          <li class="">
            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <img src="{{ url('') }}/design/images/img.jpg" alt="">{{ admin()->user()->name }}
              <span class=" fa fa-angle-down"></span>
            </a>
            <ul class="dropdown-menu dropdown-usermenu pull-right">
              <li><a href="javascript:;"> Profile</a></li>
              <li>
                <a href="javascript:;">
                  <span class="badge bg-red pull-right">50%</span>
                  <span>Settings</span>
                </a>
              </li>
              <li><a href="javascript:;">Help</a></li>
              <li><a href="{{ aurl('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
            </ul>
          </li>
          {{-- profile --}}

          {{-- language --}}

            <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class=" fa fa-globe"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a href="{{ aurl('lang/ar') }}"><i class="fa fa-flag"></i> عربى</a></li>
                  <li><a href="{{ aurl('lang/en') }}"><i class="fa fa-flag"></i> English</a></li>
                </ul>
            </li>

            {{-- language --}}

          <li role="presentation" class="dropdown">
            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
              <i class="fa fa-envelope-o"></i>
              <span class="badge bg-green">6</span>
            </a>
            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
              <li>
                <a>
                  <span class="image"><img src="{{ url('') }}/design/images/img.jpg" alt="Profile Image" /></span>
                  <span>
                    <span>John Smith</span>
                    <span class="time">3 mins ago</span>
                  </span>
                  <span class="message">
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <a>
                  <span class="image"><img src="{{ url('') }}/design/images/img.jpg" alt="Profile Image" /></span>
                  <span>
                    <span>John Smith</span>
                    <span class="time">3 mins ago</span>
                  </span>
                  <span class="message">
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <a>
                  <span class="image"><img src="{{ url('') }}/design/images/img.jpg" alt="Profile Image" /></span>
                  <span>
                    <span>John Smith</span>
                    <span class="time">3 mins ago</span>
                  </span>
                  <span class="message">
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <a>
                  <span class="image"><img src="{{ url('') }}/design/images/img.jpg" alt="Profile Image" /></span>
                  <span>
                    <span>John Smith</span>
                    <span class="time">3 mins ago</span>
                  </span>
                  <span class="message">
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <div class="text-center">
                  <a>
                    <strong>See All Alerts</strong>
                    <i class="fa fa-angle-right"></i>
                  </a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <!-- /top navigation -->
