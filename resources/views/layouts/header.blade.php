<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ !empty($title)?$title:'Admin' }}</title>

        <!-- Bootstrap -->
    <link href="{{ url('') }}/design/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('') }}/design/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('') }}/design/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ url('') }}/design/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ url('') }}/design/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ url('') }}/design/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ url('') }}/design/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Datatables -->
    {{-- <link href="{{ url('') }}/design//build/css/datatables.min.css" rel="stylesheet"> --}}
    <link href="{{ url('') }}/design/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('') }}/design/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('') }}/design/build/dataTableCss/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('') }}/design/build/dataTableCss/buttons.dataTables.min.css" rel="stylesheet">

    <link href="{{ url('') }}/design/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('') }}/design/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('') }}/design/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{ url('') }}/design/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{{ url('') }}/design/build/js/jquery.form-validator.min.js"></script>   
<!-- Custom Theme Style -->
<link href="{{ url('') }}/design/build/css/flag-icon.min.css" rel="stylesheet">
@if (direction() == 'ltr')
  <link href="{{ url('') }}/design/build/css/custom.min.css" rel="stylesheet">
@else
<link href="{{ url('') }}/design/build/css/rtl/custom.min.css" rel="stylesheet">
<link href="{{ url('') }}/design/build/css/rtl/bootstrap-rtl.css" rel="stylesheet">
    <link href="{{ url('') }}/design/build/css/rtl/font/cairoFont.css" rel="stylesheet">
    <style>
        html,body,.alert{
            font-family: 'Cairo', sans-serif;
        }
    </style>
@endif
    <script src="{{ url('') }}/design/build/js/sweetalert.js"></script>
    <script src="{{ url('') }}/design/build/js/sweetalert2@8.js"></script>
    <script src="{{url('') }}/design//build/js/datatables.min.js"></script>

    {{--noty--}}
    <link rel="stylesheet" href="{{ asset('design/build/js/noty/noty.css') }}">
    <script src="{{ asset('design/build/js/noty/noty.min.js') }}"></script>

    @stack('css')
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
          @include('partials._errors')
          @include('partials._session')