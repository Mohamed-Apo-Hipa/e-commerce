<!-- footer content -->
<footer>
  <div class="pull-right">
    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>


<!-- Bootstrap -->
<script src="{{ url('') }}/design/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="{{ url('') }}/design/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="{{ url('') }}/design/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="{{ url('') }}/design/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="{{ url('') }}/design/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="{{ url('') }}/design/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="{{ url('') }}/design/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="{{ url('') }}/design/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="{{ url('') }}/design/vendors/Flot/jquery.flot.js"></script>
<script src="{{ url('') }}/design/vendors/Flot/jquery.flot.pie.js"></script>
<script src="{{ url('') }}/design/vendors/Flot/jquery.flot.time.js"></script>
<script src="{{ url('') }}/design/vendors/Flot/jquery.flot.stack.js"></script>
<script src="{{ url('') }}/design/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="{{ url('') }}/design/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="{{ url('') }}/design/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="{{ url('') }}/design/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="{{ url('') }}/design/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="{{ url('') }}/design/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="{{ url('') }}/design/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="{{ url('') }}/design/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{ url('') }}/design/vendors/moment/min/moment.min.js"></script>
<script src="{{ url('') }}/design/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datatables -->
<script src="{{ url('') }}/design/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{ url('') }}/design/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{ url('') }}/design/build/dataTableJS/buttons.min.js"></script>
<script src="{{ url('') }}/design/build/dataTableJS/buttons.bootstrap.min.js"></script>
<script src="{{ url('') }}/design/build/dataTableJS/buttons.html5.js"></script>
<script src="{{ url('') }}/design/build/dataTableJS/buttons.flash.min.js"></script>
<script src="{{ url('') }}/design/build/dataTableJS/buttons.colVis.min.js"></script>
<script src="{{ url('') }}/design/build/dataTableJS/buttons.print.min.js"></script>
<script src="{{ url('') }}/design/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="{{ url('') }}/design/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ url('') }}/design/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="{{ url('') }}/design/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="{{ url('') }}/design/vendors/jszip/dist/jszip.min.js"></script>
<script src="{{ url('') }}/design/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="{{ url('') }}/design/vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="{{ url('') }}/design/build/js/custom.min.js"></script>
<script src="{{ url('') }}/design/build/js/myFunction.js"></script>

{{--custom js--}}
<script src="{{ asset('js/custom/image_preview.js') }}"></script>

<script>
  $(document).ready(function(){
      //   //icheck
      //   $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      //     checkboxClass: 'icheckbox_minimal-blue',
      //     radioClass: 'iradio_minimal-blue'
      // });

       //delete
       $(document).on('click','.delete',function(e){
          var that = $(this)
          e.preventDefault();
          var n = new Noty({
              text: "@lang('admin.confirm_delete')",
              type: "warning",
              killer: true,
              buttons: [
                  Noty.button("@lang('admin.yes')", 'btn btn-danger mr-2', function () {
                      that.closest('form').submit();
                  }),

                  Noty.button("@lang('admin.no')", 'btn btn-primary mr-2', function () {
                      n.close();
                  })
              ]
          });
          n.show();
       });

      }); //end of ready
</script>
<script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script> 
@stack('js')
</body>
</html>
