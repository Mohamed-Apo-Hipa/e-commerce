<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .content{
            width: 60%;
            margin: 20px auto;
            background: #fcfcfc;
            border: 1px solid #FFF;
        text-align: center;
        }
    </style>
</head>
<body style="background:#f6f6f6;">
<div class="content">
    {!! setting()->message_maintenance !!}
</div>
</body>
</html>