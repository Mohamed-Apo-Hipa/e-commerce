<?php
use Illuminate\Support\Facades\Route;

    Route::group(['prefix' => 'admin','namespace'=>'Admin'], function() {

        Config::set('auth.defines', 'admin');
        Route::get('login',function(){ return view('admin/login');});
        Route::post('login','AdminAuth@loginPost');
        Route::get('forgot',function(){return view('admin/forgot');});
        Route::post('forgot','AdminAuth@forgot');
        Route::get('reset/password/{token}','AdminAuth@reset_password');
        Route::post('reset/password/{token}','AdminAuth@reset_password_final');

        Route::group(['middleware' => 'admin:admin'], function() {

            Route::get('/', function () {return view('index');});
            Route::any('logout','AdminAuth@logout');

            //admins routs
            Route::resource('admin','AdminController');
            Route::delete('admin/destroy/all', 'AdminController@multi_delete');

            //users routs
            Route::resource('user','UsersController');
            Route::delete('user/destroy/all', 'UsersController@multi_delete');
            
            //settings routs
            Route::resource('settings', 'Settings');

        });
        
        // language route
        Route::get('lang/{lang}' , function($lang){
            session()->has('lang')?session()->forget('lang') : '';
            $lang == 'ar' ? session()->put('lang' , 'ar') : session()->put('lang', 'en');
            return back();

        }); // end of lang route
    
    });

