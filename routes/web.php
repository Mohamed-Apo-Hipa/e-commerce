<?php

  Route::group(['middleware' => 'Maintenance'], function () {
      Route::get('/', function () {
          return view('style.layouts.app');
      });
  });  

  Route::get('maintenance', function(){
      if(setting()->status == 'open'){
          return redirect('/');
      }
    return view('style/layouts/maintenance');
  });

