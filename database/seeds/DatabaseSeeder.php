<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       DB::table('admins')->insert([
           'name'=>'Mohamed Adel',
           'email'=>'mohamedadel31038@gmail.com',
           'password'=>bcrypt('112233')
       ]);
    }
}
