<?php

if(!function_exists('setting')){
    function setting(){
        return  \App\Model\Setting::orderBy('id','desc')->first();
    }
}

if(!function_exists('aurl')){
    function aurl($url = null){
        return url('admin/'.$url);
    }
}

//  lang
    
if (!function_exists('lang')) {
	function lang() {
		if (session()->has('lang')) {
			return session('lang');
		} else {
			session()->put('lang', setting()->main_lang);
			return setting()->main_lang;
		}
	}
}
//  end of lang function

// start of dir function
if (!function_exists('direction')) {
	function direction() {
		if (session()->has('lang')) {
			if (session('lang') == 'ar') {
				return 'rtl';
			} else {
				return 'ltr';
			}
		} else {
			return 'ltr';
		}
	}
}

if(!function_exists('admin')){
    function admin(){
        return auth()->guard('admin');
    }
}


if(!function_exists('dataTable_lang')){
    function dataTable_lang(){
        return [
            'sProcessing'=>trans('admin.sProcessing'),
            'sLengthMenu'=>trans('admin.sLengthMenu'),
            'sZeroRecords'=>trans('admin.sZeroRecords'),
            'sEmptyTable'=>trans('admin.sEmptyTable'),
            'sInfo'      =>trans('admin.sInfo'),
            'sInfoEmpty'      =>trans('admin.sInfoEmpty'),
            'sInfoFiltered'      =>trans('admin.sInfoFiltered'),
            'sInfoPostFix'      =>trans('admin.sInfoPostFix'),
            'sSearch'      =>trans('admin.sSearch'),
            'sUrl'      =>trans('admin.sUrl'),
            'sInfoThousands'      =>trans('admin.sInfoThousands'),
            'sLoadingRecords'      =>trans('admin.sLoadingRecords'),
            'oPaginate'      =>[
                'sFirst'      =>trans('admin.sFirst'),
                'sLast'      =>trans('admin.sLast'),
                'sNext'      =>trans('admin.sNext'),
                'sPrevious'      =>trans('admin.sPrevious'),
            ],
            'oAria'=>[
                'sSortAscending'      =>trans('admin.sSortAscending'),
                'sSortDescending'      =>trans('admin.sSortDescending'),
            ],
        ];
    }

}

// validate Helper functions

    if(!function_exists('v_image')){
        function v_image($ext = null)
        {
            if($ext === null){
                return 'image|mimes:jpg,jpeg,png,gif,bmp';
            }else{
                return 'image|mimes:'.$ext;
            }
        }
    }

// validate Helper functions

// upload single image 

    if(!function_exists('upload'))
    {
        function upload($data=[]){

            if(in_array('new_name', $data)){
                $new_name = $data['new_name'] === null ? time(): $data['new_name'];
            }

            if(request()->hasFile($data['file']) && $data['upload_type'] == 'single'){

                Storage::has($data['delete_file']) ? Storage::delete($data['delete_file']) : '';
                return request()->file($data['file'])->store($data['path']);
            }

        }
    }

// upload single image 
