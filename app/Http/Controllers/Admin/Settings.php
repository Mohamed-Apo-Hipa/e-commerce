<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Setting;
use Illuminate\Support\Facades\Storage;

class Settings extends Controller
{
    public function  index()
    {
        return view('admin.settings',['title' => __('admin.setting')] );
    }

    public function store()
    {
        $data = $this->validate(request(), [
            'logo' => v_image(),
            'icon'=> v_image(),
            'status'              => 'required',
            'description'         => '',
            'keywords'            => '',
            'main_lang'           => '' ,
            'message_maintenance' => '',
            'email'               => '',
            'sitename_en'         => 'required',
            'sitename_ar'         => 'required',
        ],[],
            [
            'logo'=>__('admin.logo'),
            'icon'=>__('admin.icon'),
            'status'              => __('admin.status'),
            'sitename_en'         => __('admin.sitename_en'),
            'sitename_ar'         => __('admin.sitename_ar'),
            ]); //end validate

        if(request()->hasFile('logo')){
            $data['logo'] = upload([
                'file'          =>  'logo',
                'path'          =>  'settings',
                'upload_type'   =>  'single',
                'delete_file'   =>  setting()->logo,
            ]);
        }

        if(request()->hasFile('icon')){
            $data['icon'] = upload([
                'file'          =>  'icon',
                'path'          =>  'settings',
                'upload_type'   =>  'single',   
                'delete_file'   =>  setting()->icon,
            ]);
        }

        setting::orderBy('id','desc')->update($data);
        session()->flash('success',__('admin.edited_successfully'));
        return redirect(aurl('settings'));
    }
}
