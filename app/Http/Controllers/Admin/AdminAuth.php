<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use DB;
use Mail;
use Alert;
use Carbon\carbon;
use App\Mail\AdminResetPassword;
class AdminAuth extends Controller
{
    public function loginPost(){
        $remeber =  request('remeber') == 1? true : false;
        if(admin()->attempt(['email'=>request('email'),'password'=>request('password')],$remeber))
        {
            return redirect('admin');
        }else{

           return redirect('admin/login');
        }
    }
    public function  logout(){
        admin()->logout();
        return redirect('admin/login');
    }
    public function  forgot()
    {
        $admin=  Admin::where('email',request('email'))->first();
        if(!empty($admin))
        {
            $token = app('auth.password.broker')->createToken($admin);
            $data = DB::table('password_resets')->insert([
                'email'=>$admin->email,
                'token'=>$token,
                'created_at'=>carbon::now(),
            ]);
            Mail::to($admin->email)->send(new AdminResetPassword(['data'=>$admin,'token'=>$token]));
            alert()->success('Success', 'Email is sent Check your inbox')->autoclose(3500);
            return back();
        }
        alert()->error('Error', 'we not have this email');
        return back();
    }
    public function  reset_password($token)
    {
        $data = DB::table('password_resets')->where('token',$token)->where('created_at','>',carbon::now()->subHours(2))->first();
        if(!empty($data))
        {
            return view('admin.resetPassword',compact('data'));

        }else{
            return view('admin.forgot');
        }
    }
    public function  reset_password_final($token)
    {
        $this->validate(request(),[
            'password'=>'required|confirmed',
            'password_confirmation'=>'required',
        ]);

        $data = DB::table('password_resets')->where('token',$token)->where('created_at','>',carbon::now()->subHours(2))->first();
        if(!empty($data))
        {
            $admin = Admin::where('email', $data->email)->update([
                'email' => $data->email,
                'password' => bcrypt(request('password'))
            ]);
            DB::table('password_resets')->where('email', request('emai'))->delete();
            admin()->attempt(['email'=>$data->email,'password'=>request('password')],true);
            return redirect(aurl());

        }else{
            return redirect(aurl('forgot'));
        }
    }
    

}
