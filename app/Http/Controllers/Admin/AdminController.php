<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\controller;
use App\DataTables\AdminDatatable;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Admin;
use function Matrix\trace;

class AdminController extends Controller
{
  
    public function index(AdminDatatable $admin)
    {
        return $admin->render('admin.admins.index', ['title'=>trans('admin.adminControl')]);
    }

    public function create()
    {
        return view('admin.admins.create', ['title'=>trans('admin.create_admin')]);
    }


    public function store(Request $request)
    {
        $data = $this->validate(request(),[
            'name'  => 'required',
            'email'  => 'required|email|unique:admins',
            'password'  => 'required|min:6'
        ],[],[
            'name' => trans('admin.name'),
            'email' => trans('admin.email'),
            'password' => trans('admin.password'),
            ]);
            $data['password'] = bcrypt(request('password'));
            Admin::create($data);
            session()->flash('success',__('admin.added_successfully'));
            return redirect(aurl('admin')); 
    }

    public function edit($id)
    {
        $admin = Admin::find($id);
        $title  = trans('admin.edit');
        return view('admin.admins.edit',compact('admin','title'));
    }
    public function update(Request $request, Admin $admin)
    {
    
            $data = $this->validate(request(),[
                'name'  => 'required',
                'email'  => 'required|email|unique:admins,email,'.$admin->id,
                'password'  => 'sometimes|nullable|min:6'
                ],[],[
                'name' => trans('admin.name'),
                'email' => trans('admin.email'),
                'password' => trans('admin.password'),
                ]);
                if($request->has('password')){
                    $data['password'] = bcrypt(request('password'));
                }
                $admin->update($data);
                session()->flash('success',__('admin.edited_successfully'));
                return redirect(aurl('admin'));
        
    }


 
    public function destroy(Admin $admin) 
    {
		$admin->delete();
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(aurl('admin'));
    }
    
    public function multi_delete()
    {
        if (is_array(request('item'))) 
        {
			Admin::destroy(request('item'));
		} else {
			Admin::find(request('item'))->delete();
		}
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(aurl('admin'));
    }
}
