<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\controller;
use App\DataTables\UserDatatable;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\User;
use function Matrix\trace;

class UsersController extends Controller
{
  
    public function index(UserDatatable $user)
    {
        return $user->render('admin.users.index', ['title'=>__('admin.users')]);
    }

    public function create()
    {
        return view('admin.users.create', ['title'=>__('admin.create_user')]);
    }


    public function store(Request $request)
    {
        $data = $this->validate(request(),[
            'name'  => 'required',
            'level'  => 'required|in:user,company,vendor',
            'email'  => 'required|email|unique:users',
            'password'  => 'required|min:6'
        ],[],[
            'name' => __('admin.name'),
            'level' => __('admin.level'),
            'email' => __('admin.email'),
            'password' => __('admin.password'),
            ]);
            $data['password'] = bcrypt(request('password'));
            User::create($data);
            session()->flash('success',__('admin.added_successfully'));
            return redirect(aurl('user')); 
    }

    public function edit($id)
    {
        $user = User::find($id);
        $title  = __('admin.edit');
        return view('admin.users.edit',compact('user','title'));
    }
    public function update(Request $request, User $user)
    {
            $data = $this->validate(request(),[
                'name'  => 'required',
                'level'  => 'required|in:user,company,vendor',
                'email'  => 'required|email|unique:users,email,'.$user->id,
                'password'  => 'sometimes|nullable|min:6'
                ],[],[
                'name' => __('admin.name'),
                'level' => __('admin.level'),
                'email' => __('admin.email'),
                'password' => __('admin.password'),
                ]);
                if($request->has('password')){
                    $data['password'] = bcrypt(request('password'));
                }
                $user->update($data);
                session()->flash('success',__('admin.edited_successfully'));
                return redirect(aurl('user'));
        
    }
    public function destroy(User $user) 
    {
		$user->delete();
		session()->flash('success', __('admin.deleted_record'));
		return redirect(aurl('user'));
    }
    
    public function multi_delete()
    {
        if (is_array(request('item'))) 
        {
			User::destroy(request('item'));
		} else {
			User::find(request('item'))->delete();
		}
		session()->flash('success', __('admin.deleted_record'));
		return redirect(aurl('user'));
    }
}
