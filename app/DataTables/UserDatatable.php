<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;

class UserDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('checkbox', 'admin.users.btn.checkbox')
            ->addColumn('edit', 'admin.users.btn.edit')
            ->addColumn('delete', 'admin.users.btn.delete')
            ->addColumn('level', 'admin.users.btn.level')
            ->rawColumns([
                'edit',
				'delete',
				'checkbox',
				'level',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Admin $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return User::query()->where(function($q){
            if(request()->has('level')){
                return $q->where('level', request('level'));
            }
        });
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'lengthMenu'    => [[10,20,30,40,-1],[10,20,30,40,'All Record']],
                        'buttons'       =>  [
                            [
                                'text'=>'<i class="fa fa-trash"></i> '.trans('admin.delete_all'),
                                'className' =>'btn btn-danger delBtn'
                            ],
                            ['extend'   => 'print','className'=>'btn btn-primary','text'=>'<i class="fa fa-print"></i>' ],
                            ['extend'   => 'csv','className'=>'btn btn-info','text'=>'<i class="fa fa-file"></i> '.trans('admin.ex_csv') ],
                            ['extend'   => 'excel','className'=>'btn btn-success','text'=>'<i class="fa fa-file"></i> '.trans('admin.ex_excel') ],
                            [
                                'text'=>'<i class="fa fa-plus"></i> '.trans('admin.create_user'),
                                'className' =>'btn btn-dark',
                                'action'=>"function(){
                                                        window.location.href = '".\URL::current()."/create'; 
                                                    }"
                            ],

                        ],
                        'language'  => dataTable_lang(),
                        'pagingType'=>'full_numbers'
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name'          =>'checkbox',
                'data'          =>'checkbox',
                'title'         =>'<input type="checkbox"  class="check_all" onclick ="check_all()" />',
                'exportable'    =>false,
                'printable'     =>false,
                'orderable'     =>false,
                'searchable'    =>false,
            ],
            [
                'name'  =>'id',
                'data'  =>'id',
                'title' =>'#'
            ],[
                'name'  =>'name',
                'data'  =>'name',
                'title' =>trans('admin.name')
            ],[
                'name'  =>'level',
                'data'  =>'level',
                'title' =>trans('admin.level')
            ],[
                'name'  =>'email',
                'data'  =>'email',
                'title' =>trans('admin.email')
            ],
            [
                'name'  =>'created_at',
                'data'  =>'created_at',
                'title' =>trans('admin.createdAt')
            ],
            [
                'name'  =>'updated_at',
                'data'  =>'updated_at',
                'title' =>trans('admin.updatedAt')
            ],[
				'name'       => 'edit',
				'data'       => 'edit',
				'title'      => trans('admin.edit'),
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			], [
				'name'       => 'delete',
				'data'       => 'delete',
				'title'      => trans('admin.delete'),
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
